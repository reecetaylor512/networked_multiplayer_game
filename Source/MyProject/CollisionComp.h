// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "CollisionComp.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API UCollisionComp : public UBoxComponent
{
	GENERATED_BODY()
	
};
