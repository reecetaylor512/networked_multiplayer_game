// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "Speed.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API ASpeed : public APickup
{
	GENERATED_BODY()

public:
	ASpeed(); 

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float newSpeed; 
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float orgSpeed; 
	
};
